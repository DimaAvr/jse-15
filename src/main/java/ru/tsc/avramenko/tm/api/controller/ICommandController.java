package ru.tsc.avramenko.tm.api.controller;

public interface ICommandController {

    void exit();

    void showInfo();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showCommandValue(String value);

    void showHelp();
}
